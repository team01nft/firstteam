
const nftContractAddress = "0x7ae5191f057ab66544f4877A951a397D8414D430".toLocaleLowerCase();
const marketContractAddress = "0x12d54855199a6A9361C798D0151825843eAD5D55".toLocaleLowerCase();
const auctionContractAddress = "0xca8641663E2658b8e09F575f5492D45918f35E1e".toLocaleLowerCase();

const nftContractAddress1155 = "0x7C13431e8695C436D7611C1750CB2a63c06C8BB5".toLocaleLowerCase();
const marketContractAddress1155 = "0x5E27327c9Ce6fcE310a540Fd7B775Fd76D76782C".toLocaleLowerCase();


const list1155contracts = [nftContractAddress1155, "0xd26e22d16113c63d8e1c3d7ee96e85944e2e786b".toLocaleLowerCase()]

module.exports = {nftContractAddress, marketContractAddress, auctionContractAddress, nftContractAddress1155, marketContractAddress1155, list1155contracts}


